package com.tic.bbq;

import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class BbqApplication {

	public static void main(String[] args) {
		SpringApplication.run(BbqApplication.class, args);
	}

}
