package com.tic.bbq.constants;


/**
 * 物流类型
 * @author jm008512
 */
public enum BusinessType{
    EIC("EIC",5);
    
    public String name;

    public int index;

    BusinessType(String name, int index) {

        this.name = name;

        this.index = index;
    }
}
