package com.tic.bbq.constants;


/**
 * 单位
 * @author jm008512
 *
 */
public enum UnitEnum{
    
    PEOPLE_MONTH("人/月", 1),
    PEOPLE_DAY("人/天", 2),
    SKU("sku", 3),
    ZHANG("张", 4),
    HOUR("小时",5);
    
    public String name;

    public int index;

    UnitEnum(String name, int index) {

        this.name = name;

        this.index = index;
    }

}
