package com.tic.bbq.dto;

import lombok.Data;



/**
 * 报价层级明细
 * @author jm008512
 *
 */
@Data
public class LevelItem{
    
    //主键Id
    private Long id;
    
    //等级名称
    private String name;
    
    //层级Id
    private Integer levelId;
    
    //父级Id
    private Integer parentId;
    
    //当时级别的层级编号  
    private String LevelOrderNo;
    
    
}
