package com.tic.bbq.dto;

import lombok.Data;

/**
 * 购物车明细
 * @author jm008512
 */
@Data
public class ShoppingCartItem{
    
    //主键ID
    private Long id;
    
    //业务机会编号
    private Long businessId;
    
    //计算因子
    private CalculFactor calculFactor;
    
    //所在等级Id
    private  Long LevelId;
    
}
