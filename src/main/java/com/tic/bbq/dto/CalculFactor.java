package com.tic.bbq.dto;

import lombok.Data;

/**
 * 报价计算属性因子
 * @author jm008512
 */
@Data
public class CalculFactor{
    
    //单位
    private Integer unit;
    
    //单价
    private Long unitPrice; 
    
    //币种
    private String currencyType;

    //汇率
    private Long exchangeRate;
    
    //原价
    private Long originalPrice;
    
    //税
    private Long tax;
    
    //是否含税
    private Boolean isTax;
    
    //折扣百分比
    private Integer discountPercen;
    
    //优惠满减
    private Long discountPrice;
    
    //实际合计金额
    private Long actualAmount;
    
    
    
    
}
